<?php
App::uses('PostsController', 'Controller');

/**
 * PostsController Test Case
 */
class PostsControllerTest extends ControllerTestCase
{

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.post',
		'app.user',
		'app.comment'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$this->Post = ClassRegistry::init('Post');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Post);
		parent::tearDown();
	}

	/**
	 * testIndex method
	 *
	 * @return void
	 */
	public function testIndex()
	{
		$result = $this->testAction(
			'/posts/index',
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('posts', $result);
		$this->assertGreaterThan(0, count($result['posts']));
		$this->assertLessThan(21, count($result['posts']));
	}

	/**
	 * testView method
	 *
	 * @return void
	 */
	public function testView()
	{
		$data = array(
			'Post' => array(
				'user_id' => 1,
				'title' => 'lorem',
				'body' => 'lorem'
			)
		);
		$this->testAction('/posts/add', array('data' => $data, 'method' => 'get'));
		$this->assertContains('/posts', $this->headers['Location']);
	}

	/**
	 * testAdd method
	 *
	 * @return void
	 */
	public function testAdd()
	{
		$data = array(
			'Post' => array(
				'user_id' => 1,
				'title' => 'lorem',
				'body' => 'lorem'
			)
		);
		$this->testAction('/posts/add', array('data' => $data, 'method' => 'get'));
		$this->assertRedirect('/posts');
		$count = $this->Post->find('all')->where($data)->count();
		$this->assertEquals(1, $count);
	}

	/**
	 * testEdit method
	 *
	 * @return void
	 */
	public function testEdit()
	{
		$postId = '1';
		$result = $this->testAction(
			'/posts/edit/' . $postId,
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('post', $result);
	}

	/**
	 * testDelete method
	 *
	 * @return void
	 */
	public function testDelete()
	{
		$postId = '3';
		$Posts = $this->generate('Posts', array(
			'models' => array(
				'Post' => array('delete')
			),
		));
		$Posts->Post
			->expects($this->once())
			->method('delete')
			->will($this->returnValue(false));
		$this->testAction(
			'/posts/delete/' . $postId,
			array(
				'method' => 'post'
			)
		);
		$this->assertStringEndsWith("/posts", $this->headers['Location']);
		$this->assertEquals(1, $this->Post->find('count', array(
			'conditions' => array(
				'Post.id' => $postId,
			),
		)));
	}

	/**
	 * testAdminIndex method
	 *
	 * @return void
	 */
	public function testAdminIndex()
	{
		$this->markTestIncomplete('testAdminIndex not implemented.');
	}

	/**
	 * testAdminView method
	 *
	 * @return void
	 */
	public function testAdminView()
	{
		$this->markTestIncomplete('testAdminView not implemented.');
	}

	/**
	 * testAdminAdd method
	 *
	 * @return void
	 */
	public function testAdminAdd()
	{
		$this->markTestIncomplete('testAdminAdd not implemented.');
	}

	/**
	 * testAdminEdit method
	 *
	 * @return void
	 */
	public function testAdminEdit()
	{
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

	/**
	 * testAdminDelete method
	 *
	 * @return void
	 */
	public function testAdminDelete()
	{
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}
}
