<?php
App::uses('UsersController', 'Controller');

/**
 * UsersController Test Case
 */
class UsersControllerTest extends ControllerTestCase
{

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.user',
		'app.comment',
		'app.post'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$this->User = ClassRegistry::init('User');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->User);
		parent::tearDown();
	}

	/**
	 * testIndex method
	 *
	 * @return void
	 */
	public function testIndex()
	{
		$result = $this->testAction(
			'/users/index',
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('users', $result);
		$this->assertGreaterThan(0, count($result['users']));
		$this->assertLessThan(21, count($result['users']));
	}

	/**
	 * testView method
	 *
	 * @return void
	 */
	public function testView()
	{
		$data = array(
			'User' => array(
				'username' => 'john',
				'password' => hash('sha256', 'password'),
				'email' => 'john@example.com',
				'role' => 1
			)
		);
		$this->testAction('/users/add', array('data' => $data, 'method' => 'get'));
		$this->assertContains('/users', $this->headers['Location']);
	}

	/**
	 * test the view method with an id that does not exists
	 *
	 * @return void
	 */
	public function testViewIdDoesNotExists()
	{
		$userId = '0';
		$this->setExpectedException('NotFoundException');
		$result = $this->testAction(
			'/users/view/' . $userId,
			array('return' => 'vars', 'method' => 'get')
		);
	}

	/**
	 * testAdd method
	 *
	 * @return void
	 */
	public function testAdd()
	{
		$user = array(
			'User' => array(
				'username' => 'john',
				'password' => hash('sha256', 'password'),
				'email' => 'john@example.com',
				'role' => 1
			)
		);
		$this->testAction('/users/add', array('data' => $user, 'method' => 'get'));
		$this->assertRedirect('/users');
		$count = $this->User->find('all')->where($user)->count();
		$this->assertEquals(1, $count);
	}

	/**
	 * testEdit method
	 *
	 * @return void
	 */
	public function testEdit()
	{
		$userId = '1';
		$result = $this->testAction(
			'/users/edit/' . $userId,
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('user', $result);
	}

	/**
	 * testDelete method
	 *
	 * @return void
	 */
	public function testDelete()
	{
		$userId = '8';
		$Users = $this->generate('Users', array(
			'models' => array(
				'User' => array('delete')
			),
		));
		$Users->User
			->expects($this->once())
			->method('delete')
			->will($this->returnValue(false));
		$this->testAction(
			'/users/delete/' . $userId,
			array(
				'method' => 'post'
			)
		);
		$this->assertStringEndsWith("/posts", $this->headers['Location']);
		$this->assertEquals(1, $this->User->find('count', array(
			'conditions' => array(
				'User.id' => $userId,
			),
		)));
	}

	/**
	 * testDeleteNotFoundException method
	 *
	 * @return void
	 */
	public function testDeleteNotFoundException()
	{
		$blogId = '0000';
		$this->setExpectedException('NotFoundException');
		$this->testAction(
			'/blogs/delete/' . $blogId,
			array(
				'method' => 'get'
			)
		);
	}

	/**
	 * testAdminIndex method
	 *
	 * @return void
	 */
	public function testAdminIndex()
	{
		$result = $this->testAction(
			'/users/index',
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('users', $result);
		$this->assertGreaterThan(0, count($result['users']));
		$this->assertLessThan(21, count($result['users']));
	}

	/**
	 * testAdminView method
	 *
	 * @return void
	 */
	public function testAdminView()
	{
		$this->markTestIncomplete('testAdminView not implemented.');
	}

	/**
	 * testAdminAdd method
	 *
	 * @return void
	 */
	public function testAdminAdd()
	{
		// 
	}

	/**
	 * testAdminEdit method
	 *
	 * @return void
	 */
	public function testAdminEdit()
	{
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

	/**
	 * testAdminDelete method
	 *
	 * @return void
	 */
	public function testAdminDelete()
	{
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}
}
