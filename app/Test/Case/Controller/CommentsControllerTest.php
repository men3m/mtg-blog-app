<?php
App::uses('CommentsController', 'Controller');

/**
 * CommentsController Test Case
 */
class CommentsControllerTest extends ControllerTestCase
{

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.comment',
		'app.user',
		'app.post'
	);

	/**
	 * setUp method
	 *
	 * @return void
	 */
	public function setUp()
	{
		parent::setUp();
		$this->Comment = ClassRegistry::init('Comment');
	}

	/**
	 * tearDown method
	 *
	 * @return void
	 */
	public function tearDown()
	{
		unset($this->Comment);
		parent::tearDown();
	}

	/**
	 * testIndex method
	 *
	 * @return void
	 */
	public function testIndex()
	{
		$result = $this->testAction(
			'/comments/index',
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('comments', $result);
		$this->assertGreaterThan(0, count($result['comments']));
		$this->assertLessThan(21, count($result['comments']));
	}

	/**
	 * testView method
	 *
	 * @return void
	 */
	public function testView()
	{
		$data = array(
			'Comment' => array(
				'user_id' => 1,
				'post_id' => 1,
				'body' => 'lorem'
			)
		);
		$this->testAction('/comments/add', array('data' => $data, 'method' => 'get'));
		$this->assertContains('/comments', $this->headers['Location']);
	}

	/**
	 * testAdd method
	 *
	 * @return void
	 */
	public function testAdd()
	{
		$data = array(
			'Comment' => array(
				'user_id' => 1,
				'post_id' => 1,
				'body' => 'lorem'
			)
		);
		$this->testAction('/comments/add', array('data' => $data, 'method' => 'get'));
		$this->assertRedirect('/comments');
		$count = $this->Comment->find('all')->where($data)->count();
		$this->assertEquals(1, $count);
	}

	/**
	 * testEdit method
	 *
	 * @return void
	 */
	public function testEdit()
	{
		$commentId = '1';
		$result = $this->testAction(
			'/comments/edit/' . $commentId,
			array('return' => 'vars', 'method' => 'get')
		);
		$this->assertArrayHasKey('comment', $result);
	}

	/**
	 * testDelete method
	 *
	 * @return void
	 */
	public function testDelete()
	{
		$commentId = '3';
		$Comments = $this->generate('Comments', array(
			'models' => array(
				'Comment' => array('delete')
			),
		));
		$Comments->Comment
			->expects($this->once())
			->method('delete')
			->will($this->returnValue(false));
		$this->testAction(
			'/comments/delete/' . $commentId,
			array(
				'method' => 'comment'
			)
		);
		$this->assertStringEndsWith("/comments", $this->headers['Location']);
		$this->assertEquals(1, $this->Comment->find('count', array(
			'conditions' => array(
				'Comment.id' => $commentId,
			),
		)));
	}

	/**
	 * testAdminIndex method
	 *
	 * @return void
	 */
	public function testAdminIndex()
	{
		$this->markTestIncomplete('testAdminIndex not implemented.');
	}

	/**
	 * testAdminView method
	 *
	 * @return void
	 */
	public function testAdminView()
	{
		$this->markTestIncomplete('testAdminView not implemented.');
	}

	/**
	 * testAdminAdd method
	 *
	 * @return void
	 */
	public function testAdminAdd()
	{
		$this->markTestIncomplete('testAdminAdd not implemented.');
	}

	/**
	 * testAdminEdit method
	 *
	 * @return void
	 */
	public function testAdminEdit()
	{
		$this->markTestIncomplete('testAdminEdit not implemented.');
	}

	/**
	 * testAdminDelete method
	 *
	 * @return void
	 */
	public function testAdminDelete()
	{
		$this->markTestIncomplete('testAdminDelete not implemented.');
	}
}
